import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
object HelloWorld {
    @JvmStatic
    fun main(args: Array<String>) {
        // Prints "Hello, World" to the terminal window.
        var emne = mqttClient("tcp://test.mosquitto.org:1883", "LYF")
        val options = MqttConnectOptions()
        options.isAutomaticReconnect = true
        options.isCleanSession = true
        options.connectionTimeout = 10
        emne.publisher.connect(options)

        val receivedSignal = CountDownLatch(10)
        emne.publisher.subscribe("LYF", { topic, msg ->
            val payload = String(msg.getPayload())
            println(payload)
            receivedSignal.countDown()
        })
        receivedSignal.await(1, TimeUnit.MINUTES)
    }
}