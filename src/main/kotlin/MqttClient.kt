import org.eclipse.paho.client.mqttv3.IMqttClient
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttMessage
import java.util.concurrent.Callable
import org.eclipse.paho.client.mqttv3.logging.LoggerFactory
import kotlin.random.Random


class mqttClient(val serverUri:String,val clientId:String){
    val publisher : IMqttClient = MqttClient(serverUri,clientId)

}